=== Disable Some Site Healt Tests ===

* Contributors: baxtian
* Tags: Site Healt, Disable tests, HTTPS, Debug
* Requires at least: 5.4
* Tested up to: 5.4
* Stable tag: 0.0.1
* Requires PHP: 5.6
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html

Disables HTTPS and DEBUG tests because we are not in an *https server* and we are *debuging*.
