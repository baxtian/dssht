<?php
/*
Plugin Name: Disable Some Site Healt Tests
Description: Simply disable https and debug tests in dev environment becaues we ar enot in an https server and we are debugin.
Author: baxtian
Version: 0.0.1

GNU General Public License, Free Software Foundation <http://creativecommons.org/licenses/GPL/2.0/>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

add_filter('site_status_tests', 'dssht');

function dssht($tests)
{
	//Deshabilitar pruebas https y debug
	unset($tests['async']['https_status'], $tests['direct']['debug_enabled']);

	return $tests;
}
